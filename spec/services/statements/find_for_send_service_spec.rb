# frozen_string_literal: true

require 'rails_helper'
RSpec.describe 'Statements::FindForSendService', type: :service do
  describe 'Call method' do
    before(:each) do
      create_another_users_statements
    end

    it 'Return correct statement #1' do
      statement = create_statement(0, nil)
      create_statement(0, DateTime.current - 1.hour, statement.user)
      create_statement(50, DateTime.current - 1.day, statement.user)
      create_statement(100, DateTime.current - 2.day, statement.user)

      result_statement = Statements::FindForSendService.call(statement.user)

      expect(result_statement.id).to(eq(statement.id))
    end

    it 'Return correct statement #2' do
      statement = create_statement(100, DateTime.current - 1.day)
      create_statement(100, DateTime.current - 2.day, statement.user)

      result_statement = Statements::FindForSendService.call(statement.user)

      expect(result_statement).to(be_nil)
    end

    it 'Return correct statement #3' do
      user = create(:user)

      result_statement = Statements::FindForSendService.call(user)

      expect(result_statement).to(be_nil)
    end

    it 'Return correct statement #4' do
      statement = create_statement(10, DateTime.current - 1.hour)
      create_statement(25, DateTime.current - 1.hour, statement.user)
      create_statement(50, DateTime.current - 1.day, statement.user)
      create_statement(100, DateTime.current - 2.day, statement.user)

      result_statement = Statements::FindForSendService.call(statement.user)

      expect(result_statement.id).to(eq(statement.id))
    end

    it 'Return correct statement #5' do
      statement = create_statement(25, DateTime.current - 2.hour)
      create_statement(25, DateTime.current - 1.hour, statement.user)
      create_statement(50, DateTime.current - 1.day, statement.user)
      create_statement(100, DateTime.current - 2.day, statement.user)

      result_statement = Statements::FindForSendService.call(statement.user)

      expect(result_statement.id).to(eq(statement.id))
    end
  end

  def create_statement(understood_percent, shown_at, user = nil)
    create(:statement, {understood_percent: understood_percent, shown_at: shown_at, user: user}.compact)
  end

  def create_another_users_statements
    create_statement(0, nil)
    create_statement(0, DateTime.current - 1.hour)
    create_statement(50, DateTime.current - 1.day)
    create_statement(100, DateTime.current - 2.day)
  end
end
