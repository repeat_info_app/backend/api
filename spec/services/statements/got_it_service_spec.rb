# frozen_string_literal: true

require 'rails_helper'
RSpec.describe 'Statements::GotItService', type: :service do
  describe 'Call method' do
    before(:each) do
      create_some_statements
    end

    it 'Update statement #1' do
      statement = create_statement(0)

      result_statement = Statements::GotItService.call(statement)

      aggregate_failures('Testing result') do
        expect(result_statement.id).to(eq(statement.id))
        expect(result_statement.understood_percent).to(eq(33))
      end
    end

    it 'Update statement #2' do
      statement = create_statement(33)

      result_statement = Statements::GotItService.call(statement)

      aggregate_failures('Testing result') do
        expect(result_statement.id).to(eq(statement.id))
        expect(result_statement.understood_percent).to(eq(66))
      end
    end

    it 'Update statement #3' do
      statement = create_statement(66)

      result_statement = Statements::GotItService.call(statement)

      aggregate_failures('Testing result') do
        expect(result_statement.id).to(eq(statement.id))
        expect(result_statement.understood_percent).to(eq(100))
      end
    end

    it 'Update statement #4' do
      statement = create_statement(100)

      result_statement = Statements::GotItService.call(statement)

      aggregate_failures('Testing result') do
        expect(result_statement.id).to(eq(statement.id))
        expect(result_statement.understood_percent).to(eq(100))
      end
    end
  end

  def create_statement(understood_percent)
    create(:statement, understood_percent: understood_percent)
  end

  def create_some_statements
    3.times do
      create(:statement)
    end
  end
end
