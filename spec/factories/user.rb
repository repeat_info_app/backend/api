require 'faker'

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    external_id { Faker::Number.number(digits: 10) }
    token { Faker::Number.number(digits: 10) }
  end
end
