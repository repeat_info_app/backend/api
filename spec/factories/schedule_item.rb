FactoryBot.define do
  factory :schedule_item do
    time { '12:00' }
    days { %w[monday tuesday wednesday thursday friday saturday sunday] }
    user
  end
end
