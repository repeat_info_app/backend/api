require 'faker'

FactoryBot.define do
  factory :statement do
    text { Faker::Beer.name }
    image { 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAFklEQVR42mP8z8AARMQDxlENoxqwAwCVTRf12sVPTgAAAABJRU5ErkJggg==' }
    user
  end
end
