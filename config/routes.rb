# frozen_string_literal: true

Rails.application.routes.draw do
  # Authorization
  post 'sign_in', to: 'authorization#sign_in', as: 'sign_in'
  post 'registration', to: 'authorization#registration', as: 'registration'

  # Users
  get 'user', to: 'users#show', as: 'user'
  put 'user', to: 'users#update', as: 'user_update'

  resources :statements, except: %i[edit] do
    post 'got_it'
  end
end
