# frozen_string_literal: true

module Statements
  class GotItService < ApplicationService
    Contract ClassName[Statement] => Any
    def initialize(statement)
      @statement = statement
    end

    Contract ClassName[Statement]
    def call
      return statement if statement.understood_percent == 100

      statement.update!(understood_percent: understood_percent)
      statement
    end

    private

    PERCENT_PER_CALL = 33

    attr_reader :statement

    def understood_percent
      @understood_percent ||= begin
        result = statement.understood_percent + PERCENT_PER_CALL
        result = 100 if result == 99
        result
      end
    end
  end
end
