# frozen_string_literal: true

module Statements
  class FindForSendService < ApplicationService
    Contract ClassName[User] => Any
    def initialize(user)
      @user = user
    end

    Contract Maybe[ClassName[Statement]]
    def call
      user.statements
          .where.not(understood_percent: 100)
          .order('understood_percent, shown_at is not null, shown_at')
          .first
    end

    private

    attr_reader :user
  end
end
