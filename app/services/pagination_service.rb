# frozen_string_literal: true

class PaginationService < ApplicationService
  PAGINATION_STRUCT = Struct.new(:relation, :options)

  Contract ParentClassName[ApplicationRecord], Num, Proc => Any
  def initialize(relation, page, &block)
    @relation = relation
    @page = page
    @block = block
  end

  Contract PAGINATION_STRUCT
  def call
    PAGINATION_STRUCT.new(paginated_relation, options)
  end

  private

  attr_reader :relation, :page, :block

  def paginated_relation
    @paginated_relation ||= relation.page(page)
  end

  # rubocop:disable Metrics/AbcSize
  def options
    @options ||= {
      links: {
        prev: paginated_relation.prev_page.nil? ? nil : block.call(paginated_relation.prev_page),
        current: block.call(page),
        next: paginated_relation.next_page.nil? ? nil : block.call(paginated_relation.next_page),
        count: paginated_relation.count,
        total: paginated_relation.total_count,
      }.compact,
    }
  end
  # rubocop:enable Metrics/AbcSize
end
