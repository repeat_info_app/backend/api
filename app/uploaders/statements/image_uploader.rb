# frozen_string_literal: true

module Statements
  class ImageUploader < CarrierWave::Uploader::Base
    storage :file

    def store_dir
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end

    def extension_whitelist
      %w[jpg jpeg png]
    end

    def asset_host
      ENV['HOSTNAME']
    end
  end
end
