# frozen_string_literal: true

class UserSerializer
  include FastJsonapi::ObjectSerializer

  attribute :notification_type
end
