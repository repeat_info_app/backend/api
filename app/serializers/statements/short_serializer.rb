# frozen_string_literal: true

module Statements
  class ShortSerializer
    include FastJsonapi::ObjectSerializer

    set_type 'statement_short'

    attributes :text, :understood_percent, :user_id, :created_at
  end
end
