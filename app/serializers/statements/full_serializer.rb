# frozen_string_literal: true

module Statements
  class FullSerializer
    include FastJsonapi::ObjectSerializer

    set_type 'statement_full'

    attributes :text, :understood_percent, :user_id, :image
  end
end
