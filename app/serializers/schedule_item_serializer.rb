# frozen_string_literal: true

class ScheduleItemSerializer
  include FastJsonapi::ObjectSerializer

  attributes :show_time, :days, :user_id
end
