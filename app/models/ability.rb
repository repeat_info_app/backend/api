# frozen_string_literal: true

class Ability
  include CanCan::Ability

  # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
  def initialize(user)
    # Authorization
    can :sign_in, :authorization
    can :registration, :authorization

    return if user.nil?

    # Array
    can :read_multiple, Array do |array|
      array.all? { |item| can?(:read, item) }
    end

    # Current user
    can :show, :user
    can :update, :user

    # Statements
    can :read, Statement, user_id: user.id
    can :create, :statement
    can :update, Statement, user_id: user.id
    can :destroy, Statement, user_id: user.id
    can :got_it, Statement, user_id: user.id
  end
  # rubocop:enable Metrics/AbcSize, Metrics/MethodLength
end
