# frozen_string_literal: true

class Statement < ApplicationRecord
  belongs_to :user

  validates :understood_percent,
            presence: true,
            numericality: {
              greater_than_or_equal_to: 0,
              less_than_or_equal_to: 100,
            }

  mount_base64_uploader :image, Statements::ImageUploader
end
