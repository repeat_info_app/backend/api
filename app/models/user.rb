# frozen_string_literal: true

class User < ApplicationRecord
  NOTIFICATION_TYPES = %i[mobile email].freeze

  has_many :statements
  has_many :schedule_items

  validates :email, format: { with: /\A.*?@\w*?\.\w*\z/ }, if: -> { email.present? }

  enum notification_type: NOTIFICATION_TYPES
end
