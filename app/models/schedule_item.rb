# frozen_string_literal: true

class ScheduleItem < ApplicationRecord
  DAYS = %w[monday tuesday wednesday thursday friday saturday sunday].freeze

  belongs_to :user

  validate :validate_days

  private

  def validate_days
    errors.add(:days, 'Wrong format') if days.uniq.count > 7 || (days - DAYS).present? || days.count != days.uniq.count
  end
end
