# frozen_string_literal: true

class UsersController < ApplicationController
  authorize_resource class: false

  def show
    render(json: UserSerializer.new(current_user))
  end

  def update
    current_user.update!(user_params)
    render(json: UserSerializer.new(current_user))
  end

  private

  def user_params
    params.require(:user).permit(:notification_type)
  end
end
