# frozen_string_literal: true

class AuthorizationController < ApplicationController
  authorize_resource class: false

  def sign_in
    user = User.find_by!(external_id: sign_in_params[:external_id])
    user.update!(token: sign_in_params[:token])
    render(json: UserSerializer.new(user).serializable_hash)
  end

  def registration
    user = User.create!(registration_params)
    render(json: UserSerializer.new(user).serializable_hash)
  end

  private

  def sign_in_params
    @sign_in_params ||= params.permit(:external_id, :token)
  end

  def registration_params
    @registration_params ||= params.permit(:external_id, :email, :token)
  end
end
