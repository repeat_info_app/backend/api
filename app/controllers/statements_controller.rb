# frozen_string_literal: true

class StatementsController < ApplicationController
  load_and_authorize_resource except: %i[create]

  def index
    pagination = PaginationService.call(current_user.statements, current_page) do |page|
      statements_url(page: page)
    end
    authorize!(:read_multiple, pagination.relation.to_a)

    render(json: Statements::ShortSerializer.new(pagination.relation, pagination.options))
  end

  def show
    statement = permit_statement(:read)

    render(json: Statements::FullSerializer.new(statement))
  end

  def create
    authorize!(:create, :statement)

    statement = current_user.statements.create!(statements_params)
    render(json: Statements::FullSerializer.new(statement))
  end

  def update
    statement = permit_statement(:update)

    statement.update!(statements_params)
    render(json: Statements::FullSerializer.new(statement))
  end

  def destroy
    statement = permit_statement(:destroy)

    statement.destroy!
    render(status: 204)
  end

  def got_it
    statement = permit_statement(:got_it)

    Statements::GotItService.call(statement)
    render(json: Statements::FullSerializer.new(statement))
  end

  private

  def current_page
    @current_page ||= params.permit[:page] || 1
  end

  def permit_statement(rule)
    statement = current_user.statements.find(statement_id_params[:id] || statement_id_params[:statement_id])
    authorize!(rule, statement)
    statement
  end

  def index_params
    @index_params ||= params.permit(:page)
  end

  def statement_id_params
    @statement_id_params ||= params.permit(:id, :statement_id)
  end

  def statements_params
    @statements_params ||= params.require(:statement).permit(:image, :text)
  end
end
