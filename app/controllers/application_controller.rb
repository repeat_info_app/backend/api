# frozen_string_literal: true

class ApplicationController < ActionController::API
  protected

  def current_user
    @current_user ||= User.find_by(token: request.headers['Authorization'])
  end
end
