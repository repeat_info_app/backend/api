FROM ruby:2.6.5-alpine

# Установка часового пояса
RUN apk add --update tzdata && \
    cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone

# Установка зависимостей
RUN apk add --update --virtual runtime-deps postgresql-client libffi-dev
RUN apk add --virtual build-deps build-base postgresql-dev libc-dev linux-headers libxml2-dev libxslt-dev

ENV APP_HOME /app
WORKDIR $APP_HOME

# Установка гемов
ADD Gemfile* ./
RUN bundle install --without development test

# Удаление зависимостей, которые были нужны для установки гемов
RUN apk del build-deps

# Копирование исходников
COPY . .
