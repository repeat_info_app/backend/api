# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_26_182810) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "schedule_items", force: :cascade do |t|
    t.time "show_time"
    t.jsonb "days", default: [], null: false
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_schedule_items_on_user_id"
  end

  create_table "statements", force: :cascade do |t|
    t.text "text"
    t.integer "understood_percent", default: 0, null: false
    t.datetime "shown_at"
    t.string "image"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_statements_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "external_id", null: false
    t.string "token", null: false
    t.string "email", null: false
    t.integer "notification_type", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["external_id"], name: "index_users_on_external_id", unique: true
    t.index ["token"], name: "index_users_on_token", unique: true
  end

  add_foreign_key "schedule_items", "users"
  add_foreign_key "statements", "users"
end
