class CreateStatements < ActiveRecord::Migration[6.0]
  def change
    create_table :statements do |t|
      t.text :text
      t.integer :understood_percent, null: false, default: 0
      t.datetime :shown_at
      t.string :image
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
