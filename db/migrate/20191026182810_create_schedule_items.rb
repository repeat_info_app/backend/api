class CreateScheduleItems < ActiveRecord::Migration[6.0]
  def change
    create_table :schedule_items do |t|
      t.time :show_time
      t.jsonb :days, null: false, default: []
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
