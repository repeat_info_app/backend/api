class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :external_id, null: false, index: { unique: true }
      t.string :token, null: false, index: { unique: true }
      t.string :email, null: false
      t.integer :notification_type, null: false, default: 0
      t.timestamps
    end
  end
end
