DatabaseCleaner.clean_with(:truncation, only: %i[schedule_items])
seed_file = Rails.root.join('db', 'seeds', 'development', 'schedule_items.seeds.yml')
data = YAML.load_file(seed_file)

p('schedule_items...')
ScheduleItem.create!(data)
