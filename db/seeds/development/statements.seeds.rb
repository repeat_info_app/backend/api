DatabaseCleaner.clean_with(:truncation, only: %i[statements])
seed_file = Rails.root.join('db', 'seeds', 'development', 'statements.seeds.yml')
data = YAML.load_file(seed_file)

p('statements...')
Statement.create!(data)
