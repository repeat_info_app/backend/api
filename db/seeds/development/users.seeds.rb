DatabaseCleaner.clean_with(:truncation, only: %i[users])
seed_file = Rails.root.join('db', 'seeds', 'development', 'users.seeds.yml')
data = YAML.load_file(seed_file)

p('users...')
User.create!(data)
